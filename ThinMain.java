//comments area / bottom of http://nayefreza.wordpress.com/2013/05/11/zhang-suen-thinning-algorithm-java-implementation/
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
 
/**
 *
 * @author nayef
 */
public class ThinMain {
 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
     
        BufferedImage image = ImageIO.read(new File("a.png"));
 
        int[][] imageData = new int[image.getHeight()][image.getWidth()];
        Color c;
        for (int y = 0; y < imageData.length; y++) {
            for (int x = 0; x < imageData[y].length; x++) {
 
                if (image.getRGB(x, y) == Color.BLACK.getRGB()) {
                    imageData[y][x] = 1;
                } else {
                    imageData[y][x] = 0;
 
                }
            }
        }
        
        /*
        // get a 1D array of the pixels
        int width = imageData.length;
        int height  = imageData[0].length;
        int[] pixels = new int[width * height];                          

        int s = 0;
        for(int i = 0; i < width; i ++) 
           for(int j = 0; j < height; j ++){                           
               pixels[s] = imageData[i][j];
               s++;
          } 

	ConnectComponent cc = new ConnectComponent();
	int labels[] = cc.labeling(pixels, new java.awt.Dimension(width, height), true);
                
        int array2d[][] = new int[height][width];
        for(int i=0; i<height;i++){
          for(int j=0;j<width;j++){
            array2d[i][j] = labels[(j*height) + i]; 
            System.out.print(array2d[i][j]);
          }
           System.out.println();
        }
        */
       
 
        HilditchThinning thinningService = new HilditchThinning();
     
        thinningService.doHilditchsThinning(imageData);
         
        for (int y = 0; y < imageData.length; y++) {
 
            for (int x = 0; x < imageData[y].length; x++) {
 
                if (imageData[y][x] == 1) {
                    image.setRGB(x, y, Color.BLACK.getRGB());
 
                } else {
                    image.setRGB(x, y, Color.WHITE.getRGB());
                }
 
 
            }
        }
 
        ImageIO.write(image, "png", new File("Hilditch.png"));
 
    }
}
